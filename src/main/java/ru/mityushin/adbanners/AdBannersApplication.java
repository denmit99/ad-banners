package ru.mityushin.adbanners;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdBannersApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdBannersApplication.class, args);
    }

}
