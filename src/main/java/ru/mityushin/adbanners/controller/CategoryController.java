package ru.mityushin.adbanners.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mityushin.adbanners.entity.Category;
import ru.mityushin.adbanners.service.CategoryService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService service;

    @GetMapping("")
    public List<Category> getAllCategories() {
        return service.getCategories();
    }

    @GetMapping("/{id}")
    public Category getCategoryById(@PathVariable() int id) {
        return service.getCategoryById(id);
    }

    @GetMapping("/search")
    public List<Category> getCategoryByName(@RequestParam("name") String name) {
        return service.getCategoryByName(name);
    }

    @PostMapping("")
    public Category addCategory(@RequestBody Category category) {
        return service.newCategory(category);
    }

    @PutMapping("/edit")
    public Category updateCategory(@RequestBody Category category) {
        return service.updateCategory(category);
    }

    @PutMapping("/delete/{id}")
    public String deleteCategory(@PathVariable() int id) {
        return service.deleteCategory(id);
    }
}
