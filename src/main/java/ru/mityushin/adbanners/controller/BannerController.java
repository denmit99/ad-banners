package ru.mityushin.adbanners.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mityushin.adbanners.entity.Banner;
import ru.mityushin.adbanners.service.BannerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@CrossOrigin(origins = "")
@RestController
@RequestMapping("/banner")
public class BannerController {
    @Autowired
    private BannerService service;

    @GetMapping("")
    public List<Banner> getAllBanners() {
        return service.getBanners();
    }

    @GetMapping("/{id}")
    public Banner getBannerById(@PathVariable() int id) {
        return service.getBannerById(id);
    }

    @GetMapping("/get")
    public Object getBannerTextByCategory(@RequestParam("category") String reqName, @RequestHeader(value = "User-Agent") String userAgent, HttpServletRequest request, HttpServletResponse response) {
        Banner banner = service.getBannerByCategory(reqName, userAgent, request.getRemoteAddr());
        if (banner != null)
            return banner.getContent();
        else {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/search")
    public List<Banner> getBannerByName(@RequestParam("name") String name) {
        return service.getBannerByName(name);
    }

    @PostMapping("")
    public Banner addBanner(@RequestBody Banner banner) {
        return service.newBanner(banner);
    }

    @PutMapping("/edit")
    public Banner updateBanner(@RequestBody Banner banner) {
        return service.updateBanner(banner);
    }

    @PutMapping("/delete/{id}")
    public String deleteBanner(@PathVariable() int id) {
        return service.deleteBanner(id);
    }
}
