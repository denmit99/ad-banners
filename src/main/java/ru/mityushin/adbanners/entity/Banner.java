package ru.mityushin.adbanners.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "banner")
public class Banner {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private float price;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
    private String content;
    private String deleted = "0";
}
