package ru.mityushin.adbanners.entity;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue
    private int id;
    @Column(unique = true)
    private String name;
    @Column(name = "req_name", unique = true)
    private String reqName;
    private String deleted = "0";
}
