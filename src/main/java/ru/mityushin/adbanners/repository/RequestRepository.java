package ru.mityushin.adbanners.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mityushin.adbanners.entity.Request;

public interface RequestRepository extends JpaRepository<Request, Integer> {

}
