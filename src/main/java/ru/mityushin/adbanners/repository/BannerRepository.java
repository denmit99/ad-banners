package ru.mityushin.adbanners.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.mityushin.adbanners.entity.Banner;

import java.util.List;

public interface BannerRepository extends JpaRepository<Banner, Integer> {
    @Query("select b from Banner b where b.deleted = '0'")
    List<Banner> findAllNonDeleted();

    @Query("select b from Banner b where b.deleted = '0' and lower(b.name) like %:name%")
    List<Banner> findByName(String name);

    @Query(value = "select b.* from banner b join category c on (c.id=category_id) where b.deleted='0' " +
            "and req_name = :reqName and b.id not in (select b.id from request join banner b on " +
            "(b.id=banner_id) join category c on (c.id=category_id) where " +
            "user_agent = :userAgent and ip_address = :ipAddress " +
            "and req_name = :reqName and date(date) = :date ) order by price desc", nativeQuery = true)
    List<Banner> findAllNonShowedBannersForUserInCategory(String reqName, String userAgent, String ipAddress, String date);

    @Query(value = "select b from Banner b where b.category.id = :categoryId and b.deleted = '0'")
    List<Banner> findByCategory(int categoryId);
}
