package ru.mityushin.adbanners.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.mityushin.adbanners.entity.Category;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query("select c from Category c where c.deleted = '0' and lower(c.name) like %:name%")
    List<Category> findByName(String name);

    @Query("select c from Category c where c.deleted = '0'")
    List<Category> findAllNonDeleted();
}
