package ru.mityushin.adbanners.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mityushin.adbanners.entity.Banner;
import ru.mityushin.adbanners.entity.Category;
import ru.mityushin.adbanners.repository.BannerRepository;
import ru.mityushin.adbanners.repository.CategoryRepository;

import java.util.List;
import java.util.Locale;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository repository;
    @Autowired
    private BannerRepository bannerRepository;
    
    public Category newCategory(Category category) {
        return repository.save(category);
    }

    public List<Category> getCategories() {
        return repository.findAllNonDeleted();
    }

    public Category getCategoryById(int id) {
        return repository.findById(id).orElse(null);
    }

    public List<Category> getCategoryByName(String name) {
        return repository.findByName(name.toLowerCase(Locale.ROOT));
    }

    public String deleteCategory(int id) {
        Category categoryToDelete = repository.findById(id).orElse(null);
        List<Banner> banners = bannerRepository.findByCategory(id);
        if (banners.size() == 0) {
            categoryToDelete.setDeleted("1");
            repository.save(categoryToDelete);
            return "Category [" + id + "] removed!";
        } else {
            String bannerIds = "";
            for (int i = 0; i < banners.size(); i++) {
                bannerIds += banners.get(i).getId();
                if (i != banners.size() - 1)
                    bannerIds += ", ";
            }
            return "Unable to delete this category. It referenced by banners with identifiers: " + bannerIds;
        }
    }

    public Category updateCategory(Category category) {
        Category existingCategory = repository.findById(category.getId()).orElse(null);
        existingCategory.setName(category.getName());
        existingCategory.setReqName(category.getReqName());
        return repository.save(existingCategory);
    }
}
