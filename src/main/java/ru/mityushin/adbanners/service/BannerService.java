package ru.mityushin.adbanners.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mityushin.adbanners.entity.Banner;
import ru.mityushin.adbanners.entity.Request;
import ru.mityushin.adbanners.repository.BannerRepository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BannerService {
    @Autowired
    private BannerRepository repository;
    @PersistenceContext
    private EntityManager entityManager;

    public Banner newBanner(Banner Banner) {
        return repository.save(Banner);
    }

    public List<Banner> getBanners() {
        return repository.findAllNonDeleted();
    }

    public Banner getBannerById(int id) {
        return repository.findById(id).orElse(null);
    }

    public List<Banner> getBannerByName(String name) {
        return repository.findByName(name.toLowerCase(Locale.ROOT));
    }

    @Transactional
    public Banner getBannerByCategory(String reqName, String userAgent, String ipAddress) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        List<Banner> candidates = repository.findAllNonShowedBannersForUserInCategory(reqName, userAgent, ipAddress, strDate);
        if (candidates.size() > 0) {
            float maxPrice = candidates.get(0).getPrice();
            List<Banner> filtered = candidates.stream()
                    .filter(c -> c.getPrice() == maxPrice)
                    .collect(Collectors.toList());
            Random random = new Random();
            Banner banner = filtered.get(random.nextInt(filtered.size()));
            logBannerRequest(banner, userAgent, ipAddress, date);
            return banner;
        }
        return null;
    }

    public String deleteBanner(int id) {
        Banner bannerToDelete = repository.findById(id).orElse(null);
        bannerToDelete.setDeleted("1");
        repository.save(bannerToDelete);
        return "Banner [" + id + "] removed!";
    }

    public Banner updateBanner(Banner banner) {
        Banner existingBanner = repository.findById(banner.getId()).orElse(null);
        existingBanner.setCategory(banner.getCategory());
        existingBanner.setName(banner.getName());
        existingBanner.setPrice(banner.getPrice());
        existingBanner.setContent(banner.getContent());
        return repository.save(existingBanner);
    }

    @Transactional
    public void logBannerRequest(Banner banner, String userAgent, String ipAddress, Date date){
        Request request = new Request();
        request.setBanner(banner);
        request.setDate(date);
        request.setIpAddress(ipAddress);
        request.setUserAgent(userAgent);
        entityManager.persist(request);
        entityManager.close();
    }
}
